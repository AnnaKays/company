const header = () => {
  const switcher = document.querySelector('.header__switcher');
  switcher.addEventListener('click', () => switcher.classList.toggle('header__switcher--active'));
};

export default header;
