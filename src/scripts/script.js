import Slider from '../blocks/slider/slider';
import header from '../blocks/header/header';

header();
const homeSlider = document.querySelector('.home-header .slider__slides');
if (homeSlider) {
  const addHomeSlider = new Slider(homeSlider, {
    speed: 800,
    effect: 'fade',
    autoplay: {
      delay: 5000,
      disableOnInteraction: false,
    },
    pagination: {
      el: '.slider__pagination',
      bulletClass: 'slider__pagination-item',
      bulletActiveClass: 'slider__pagination-item--active',
      clickable: true,
    },
  });
  addHomeSlider.init();
}
